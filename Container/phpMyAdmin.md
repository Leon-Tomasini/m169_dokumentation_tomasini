# phpMyAdmin
phpMyAdmin ist eine freie Webanwendung zur Administration von MySQL-Datenbanken und deren Fork MariaDB. Die Software ist in PHP implementiert; daher kommt der Name phpMyAdmin. Die meisten Funktionen können ausgeführt werden, ohne selbst SQL-Anweisungen zu schreiben, wie z. B. Wikipedia. Mit phpMyAdmin können Benutzer Tabellen und Datenbanken erstellen, löschen und bearbeiten. Sie können auch SQL-Befehle ausführen, Datenbank-Backups erstellen, Tabellenstruktur und -inhalte durchsuchen, Benutzer und Berechtigungen verwalten und vieles mehr. 
![](/Container/phpMyAdmin.jpg)
