# cAdvisor
cAdvisor (Container Advisor) ist ein Tool, das Daten zur Ressourcen-Nutzung von Docker-Containern sammelt und diese mehr oder minder hübsch aufbereitet in einer Web-UI zur Verfügung stellt. cAdvisor ist einfach zu konfigurieren und als Container ausführbar, was es sehr skalierbar macht.
![](/Container/cAdvisor.jpg)
