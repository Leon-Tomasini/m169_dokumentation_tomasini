# Arbeitsjournal Modul 169 Leon Tomasini 

 

## 24.02.2023 

Tages Ziel: 

    Ich habe mich dazu entschlossen dem Repository zu folgen,
    da ich noch kein allzu grosses Vorwissen habe und es schon etwas länger her ist,
    seitdem ich das letzte Mal mit Containern gearbeitet habe.
    Fürs erste will ich mich auf das Repository fokussieren und vielleicht dann später selber noch tiefer nachforschen. 

Tasks: 

    - Ins Repository einarbeiten 

    - Vagrant verstehen, ausprobieren, rumspielen 

    - LB2 Hands on 1–2-mal durchlaufen 

Resultat: 

    Mit dem Einarbeiten ins Repository kam ich relativ gut voran.
    Der erste Vagrant Auftrag hat gut und sauber funktioniert.
    Beim LB2 Hands on bin ich noch etwas verwirrt.
    Ich konnte die Befehle ausführen und den Webservice starten, jedoch nicht darauf zugreifen.
    Ich werde dies nächste Woche nochmals anschauen. 

 

## 03.03.2023 

    Krank 

 

## 10.03.2023 

Tages Ziel: 

    Mein Ziel war es den von letzter Woche verpassten Stoff so gut wie möglich aufzuarbeiten und 
    was übrig bleibt zu Hause fertig machen.  

Tasks: 

    - Stoff aufarbeiten 

    - Mit 20 so gut wie möglich fertig werden 

Resultat: 

    Ich bin nun mit 20 fast fertig und konnte die Aufgaben gut abschliessen.
    Vagrant verstehe ich und kann ich so weit relativ gut.
    Den Stoff von letzter Woche habe ich fast komplett aufgearbeitet und der Rest kommt noch am Wochenende dran.
    Nächste Woche will ich mit allem abschliessen. 

 

## 17.03.2023 

Tages Ziel: 

    Innerhalb der Lektionen mit 25 abschliessen. 

 

Tasks: 

    - Mit 25 abschliessen 

Resultat:  

    Ich wurde erfolgreich mit 25 fertig und ab nächster Woche werde ich mein Projekt starten und anfangen zu Doku 

 

## 24.03.2023 

Tagesziel: 

    Mit einem Projekt beginnen, informieren und evtl. Doku starten. 

Tasks: 

    - Über Azure Container-Instanzen informieren 

    - Azure Account erstellen und Students Subscription einrichten 

    - Ersten Container zum Laufen bringen 

    - Mit den Einstellungen und verschiedenen Möglichkeiten rumspielen 

Resultat: 

    Ich konnte erfolgreich einen Account einrichten und das mit der Students Subrscription funktioniert auch.
    Ich habe bis jetzt eine Ressource Group, einen Container & einen Container Registry erstellt.
    Mein Ziel für das nächste Mal ist es, ein Docker Image im Azure Container laufen zu lassen. 

 

## 31.03.2023 

Tagesziel: 

    Mein heutiges Ziel ist ein Docker Image mit Hilfe von Azure Container laufen zu lassen. 

Tasks: 

    - Docker Image auswählen 

    - Docker Image im Azure Container laufen lassen 

    - Noch mehr mit anderen Möglichkeiten/Use-Cases auseinandersetzen 

Resultat: 

    Ich konnte mein Ziel nicht erreichen, da die Variante,
    mit welcher ich das Image erstellen und in den Container “Pushen” wollte, nicht funktionierte.
    Der Grund ist,
    dass Docker Desktop nicht funktioniert auf meinem Laptop und ich Docker nicht auf meinem Windows installieren konnte.
    Als alternative habe ich kurz auf Azure eine Linux VM erstellt und da Docker installiert. 

 

## 14.04.2023 

Tagesziel: 

    Docker Container/Image zum Laufen bringen. 

Tasks: 

    - Container Instanz starten und konfigurieren 

    - Image wählen 

    - Nochmals probieren das Image von Windows zu pushen 

    - Dokumentieren 

Resultat: 

    Heute war ein einziges Desaster. Das Azure Guthaben ist wegen einem Missgeschick aufgebraucht.
    Da ich die Azure Umgebung nicht mehr nutzen kann, habe ich begonnen das ganze Lokal auf meinem Laptop umzusetzen.
    In den letzten zwei Lektionen konnte ich nun eine Debian VM Aufsetzen 
    und Docker erneut installieren (ich hatte Schwierigkeiten mit der richtigen Version). 
    Die Doku wurde nun endlich auch angefangen. Auf nächste Woche sollte auch das Repository fertig sein. 




## 21.04.2023

Tagesziel:

    Heute ist mein Ziel ein Docker Container zum laufen bringen und ein Docker Image laufen lassen. 
    Zusätzlich soll die Doku ergänzt werden und die VM noch weiter konfiguriert werden.

Tasks:

    - Image aussuchen

    - IP Adresse und weiteres anpassen

    - Docker installieren & laufen lassen

    - Dokumentieren

Resultat:

    Ich bin nun von Debian auf Ubuntu Server umgestiegen, was jedoch keinen grossen Unterschied macht für meine Ziele.
    Die IP konnte ich erfolgreich anpassen und ich konnte im Container Nextcloud laufen lassen.
    Ich habe auch probiert darauf über die Webpage zuzugreifen, was jedoch noch nicht geklappt hat.
    Um den Zugriff und die restlichen konfigs. werde ich mich in den Ferien kümmern.

## Ferien (Stand 11.05.2023)

Zeil:

    Das Ziel, war mich um den Zugriff auf Nextcloud zu kümmern und die restlichen Konfigurationen abzuschliessen.
    Zusaäztlich sollte die Dokumentation langsam zustande kommen so, 
    dass ich in den letzten Lektionen alles abschliessen kann.

Resultat/Was habe ich in den Ferien gemacht:

    Ich habe es geschaft Docker Desktop zum laufen zu bringen. Das Problem war, 
    dass mein WSL nicht auf dem neusten Stand war und zum lösen dieses Problems, musste ich es nur aktualisieren.
    Da ich nun doch Docker Desktop benutzen konnte, fing ich noch ein letztes mal von vorne an.
    Ich konnte mit Erfolg ein Docker-Compose file erstellen, in welchen ich einen MySQL und 
    einen phpMyAdmin Service (min.) konfigurierte.
    Zusätzlich habe ich noch separat einen cAdvisor Container zum laufen gebracht um die Ressourcen zu überwachen.
    Zwar kann man schon auf phpMyAdmin die Ressourcen von dem MySQL & phpMyAdmin Service überwachen,
    jedoch kann man nun alle Container überwachen mit cAdvisor.
    Ich versuchte einen Container mit node.js aufzusetzen, was jedoch einfach nicht klappen wollte.
    Mein Ziel war es einen Microservice zu konfigurieren, um Täglich ein MySQL Dump zu ziehen,
    jedoch gab ich nach etlichen Tutorials und der Bestätigung von Jakub, das es bei ihm auch nicht funktionierte, auf.
    Jetzt gilt es nur noch die Dokumentation rechtzeitig zu vollenden.


### PS:
    Die Commits sind nur wenige, da ich das Journal zuerst in Word geführt und auf OneDrive freigegeben habe,
    bevor ich es hier hin kopiert und fortgeführt habe.
