# Dokumentation_Containerumgebung

## Inhaltsverzeichnis
* 01 - Was ist ein Container?
* 02 - Was sind Microservices?
* 03 - Synchrone und Asynchrone Services
* 04 - Infrastructure as Code (IaC)
* 05 - Netzwerksicherheit
* 06 - Autorisierung &  Authentifizierung
* 07 - Docker
* 08 - Warum Container/Container Vor- & Nachteile
* 09 - Meine Umgebung
* 10 - MySQL
* 11 - phpMyAdmin
* 12 - cAdvisor
* 13 - Quellen

# 01 - Was ist ein Container?
Ein Container ist eine standardisierte, isolierte Umgebung, die Anwendungen und ihre Abhängigkeiten ausführen kann. 
Durch Container kann man Software lokal entwickeln, die woanders genauso laufen wird.
Es wird weniger aufwand für Systemabhängigkeiten und Konigurieren der Umgebung benötigt. 
Container benötigen nicht so viel Platz, wo durch man viele parallel laufen lassen kann. Man kan sie in Sekundenschelle starten und stoppen und Anwendgungen in Containern, verursachen fast keinen Overhead. 

# 02 - Was sind Microservices?
Microservices bieten eine Möglichkeit, Softwaresysteme so zu entwickeln und zu kombinieren, dass sie aus kleinen, unabhängigen Komponenten bestehen, die untereinander über das Netz kommunizieren.
Microservices ermöglichen es, statt vertikal zu skalieren (scale up), horizontal zu skalieren (skale out) und somit die Las auf mehrere Rechner zu verteilen. Die Skalierung kann sich in diesem fall auch nur auf einen einzelnen Service oder Flaschenhals beziehen.

# 03 - Synchrone und Asynchrone Services
Synchrone und Asynchrone Services sind zwei unterschiedlicher Ansättze für die Kommunikation zwischen Systemen, welche Daten austauschen möchten. 
Ein synchroner Service geht davon aus, dass der Aufrufer anhaltet und erst weiter macht, wenn er eine Antwort erhält. Mit anderen Worten der Aufrufer blockiert, bis der synchroniesierte Service seine Task abgeschlossen hat. Beispielsweise eine Web-API.
Ein asynchroner Service hingegen blockiert den Aufrufer nicht und lässt ihn weiter arbeiten, ohne dass er auf eine Antwort des Services warten muss. Die Antwort wird dann später bereitgestellt, entweder über eine Callback-Funktion oder eine Benachrichtigung an den Aufrufer.

# 04 - Infrastructure as Code (IaC)
Infrastructure as Code ist Beispiel zur Infrastruktur-Automation mittels Best Practices der Softwareentwicklung. Für die Bereitstellung von Systemen und deren Konfiguration, stehen bei IaC konsistente und wiederholbare Definitionen im Vordergrund. Dabei kommen TDD, CD, CI & VCS zum einsatz.

# 05 - Codebasierte Service-Bereitstellung
Um ein System sicherer zu machen gibt es viele möglichkeiten. Eine Website wie google.ch ist von überall aus zugänglich. Der Server auf dem diese Website läuft gehört Google. Damit nun nicht jeder über diesen Server ins Netz kann, gibt es mehrere Massnahmen, welche man treffen kann. Eine Firewall ist gut geeignet für solch eine Aufgabe, da man Ports und Protokolle sperren sowie zulassen kann, aber auch IPs und weiteres sperren und filtern kann. Eine weitere Möglichkeit ist ein Reverse Proxy. Ein Reverse Proxy holt die Ressourcen für den Client nicht lokal, sondern von anderen Servern. Durch diese Funktion, bleib die richtige Adresse des Zielsystems dem Client verborgen.

# 06 - Autorisierung und Authentifizierung
Bei der Autorisierung geht es darum, dass man nur gewisse Zugriffsrechte oder im Generellen nur gewisse Rechte hat und nicht einfach jeder User allmächtig ist. Gewisse Files oder Services sollten und sind auch nicht für jedermann verfügbar/zugänglich, da diese Personen nichts mit diesen zu tun haben oder haben sollten. Bei Top-Secret Files, wie z.B. die, die in den USA geleaked wurden, macht es kein Sinn, wenn rund 3 Millionen Leute Autorisiert sind auf diese zu zugreifen.
Bei der Authentifizierung geht es darum zu überprüfen, ob man auch wirklich die/eine autorisierte Person ist. Ein Beispiel wäre das einfache Login an einem Computer oder auf einer Website. Mit meinem Badge habe ich Zutritt zum CS gebäude wo ich arbeite, jedoch nicht an anderen Locations oder gar bei anderen Firmen, da die Authentifizierung entweder fehl schlägt oder ich nicht autorisiert bin.

![](/Dokumentation/Pictures/User-Login.png)

# 07 - Docker ![](/Dokumentation/Pictures/Docker.png)
Docker stellt ein grosses Spektrum an Container Images bereit und stellt auch ein Container Service für User zu verfügung. Docker nahm Containertechnologie von Linux auf und erweiterte so, dass eine vollständige Lösund für das Erstellen und Verteilen von Containern entstand.
Docker Desktop ist eine Software die man für Windows oder Mac herunterladen und installieren kann. Mithile Docker Desktop, kann man Images bearbeiten, pullen, pushen(auf Docker hochladen) und weiteres. Man kann auch Docker Container Laufen lassen und diese konfigurieren. Das gleiche gibt es auch als Terminal/Kommandozeile Variante. 
Es gibt auch so genannte Dockerfiles, welches eine Textadei ist mit mehreren Schritten, welche genutzt werden können, um eine Docker-Image zu erstellen. Dafür muss man zuerst ein Verzeichnis erstellen und darin ein File mit dem Namen "Dockerfile". Mit "docker build -t mysql" & "docker run --rm -d --name mysql mysql", könnte man nun eine Image builden und starten. 

# 08 - Warum Container/Container Vor- & Nachteile
Vorteile:

Container benötigen weniger Systemressourcen als herkömmliche oder Hardware-Umgebungen mit virtuellen Maschinen, da sie keine Betriebssytem-Images enthalten. 
Applikationen, die in Containern ausgeführt werden, könne ganz einfach auf mehrere verschiedene Betriebssyteme und Hardware-Plattformen implementiert werden.
DevOps-Teams wissen, dass Applikationen in Containern immer auf gleiche Weise ausgeführt werden, unabhängig davon, wo sie implementiert sind.
Container ermöglichen eine schellere Implementierung, Patching oder Skalierung von Applikationen.
Container unterstützen agile DevOps-Prozesse und beschleunigen so Entwickluings-, Test- und Produktionszyklen.

Nachteile:

Da Container in den meisten Fällen einen gemeinsamen Kernel nutzen, nämlich den Kernel des Betriebssystems, besteht generell die Gefahr, dass mehrere Container auf einmal kompromittiert werden, wenn ein Container auf einem Host angegriffen wird.
Wer mit Docker in Windows Server 2016/2019 arbeitet, muss viel in der Befehlszeile arbeiten, auf Kubernetes setzen, oder Anwendungen nutzen, die ihren Schwerpunkt nicht in der Microsoft-Welt haben. Hinzu kommt, dass die meisten Microsoft-Anwendungen nicht in Container implementierbar sind, oder dies wie im Falle von SQL-Server sehr kompliziert ist.

# 09 - Meine Umgebung
Nach etlichen Neuanfängen, habe ich schlussendlich mittels Docker Desktop, Visual Studio Code und dem Windows Terminal meine Umgebung aufgebaut. Insgesamt habe ich einen MySQL, einen phpMyAdmin und einen cAdvisor Container. Mein Ziel war es, ein Datenbank Service aufzusetzen, auf welchen man über ein Web-Interface zugreifen kann und zusätzlich ein Monitoring Service für die Container Ressourcen. Eigentlich wollte ich noch einen Container mit node.js aufsetzen, jedoch scheiterte ich mehrmals kläglich bei dem Versuch, worauf ich schlussendlich mich doch dagegen entschied.
Weiter unten finden Sie meine Konfigs und mein Vorgehen für die Container. Falls Sie mehr über die einzelnen Services wissen möchten, können Sie das auf der [Startseite](https://gitlab.com/Leon-Tomasini/m169_dokumentation_tomasini) dieses Repositorys nachsehen.

# 10 - MySQL
Der MySQL Container ist bei mir in einem Docker-Compose.yml File definiert, zusammen mit dem phpMyAdmin Container.

    docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:latest

Mit diesem Befehl, kann man einen MySQL Container erstellen und starten, zusätzlich kann man bei "MYSQL_ROOT_PASSWORD" ein Passwort für den root festlegen.

    docker ps
    
Mit diesem Befehl kann man nachsehen, ob oder welche Container am laufen sind. 

    version: '3'

    services:
      db:
        image: mysql
        restart: always
        environment:
            MYSQL_ROOT_PASSWORD: 1234
        ports:
        - "3306:3306"
        volumes:
        - ./data:/var/lib/mysql

Hier ist der erste Teil meine Docker-Compose.yml. Hier habe ich als Passwort 1234 genommen (einfach zu Merken) und den standard-Port 3306 für MySQL definiert.

# 11 - phpMyAdmin
Meine phpMyAdmin Konfiguration ist ebenfalls im DOcker-Compose.yml File.

    docker run --name phpmyadmin-container --link mysql-container:db -p 80:80 -d phpmyadmin/phpmyadmin

Dies ist der Befehl, um den phpMyAdmin Container zu erstellen & starten.

      phpmyadmin:
        image: phpmyadmin/phpmyadmin
        restart: always
        ports:
        - "80"
        environment:
        PMA_HOST: db
        MYSQL_ROOT_PASSWORD: 1234

Das Image hier ist wie erwähnt phpMyAdmin. Der ausgewählte Port ist 80, da auf 8080 cAdvisor läuft und weil ich kein Fan von komplizierten Passwoörtern bin, ist hier das Passwort wieder 1234.

# 12 - cAdvisor
Ich weiss, dass man mit phpMyAdmin bereits die Möglichkeit hat, die Ressourcen vom MySQL und phpMyAdmin Container zu überwachen, jedoch kann hat man mit cAdvisor die Möglichkeit alle Container zu überwachen (Ressourcen).

    docker run -d --name cadvisor -p 8080:8080 -v /:/rootfs:ro -v /var/run:/var/run:rw -v /sys:/sys:ro -v /var/lib/docker:/var/lib/docker:ro google/cadvisor:latest

Mit diesem Befehl erstellt man und staret man den cAdvisor Container.

# 13 - Quellen
- [M169-Repo](https://gitlab.com/ch-tbz-it/Stud/m169)
- [IP INSIDER](https://www.ip-insider.de/die-10-wichtigsten-vor-und-nachteile-von-docker-containern-a-844230/)
- [NetApp](https://www.netapp.com/de/devops-solutions/what-are-containers/)
